using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    public Vector3 RotationAxis;
    public float RotationSpeed;

    private void Update() 
    {
        RotationAxis = Vector3.ClampMagnitude(RotationAxis, 1.0f);
        RotationSpeed*=Time.deltaTime;
        RotationAxis*=RotationSpeed;
        transform.eulerAngles = RotationAxis;
    }
}
