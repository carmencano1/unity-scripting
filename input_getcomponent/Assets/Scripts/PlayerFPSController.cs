using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.ConstrainedExecution;
using UnityEngine;

public class PlayerFPSController : MonoBehaviour
{

    public GameObject cam;

    public float walkSpeed = 5f;

    public float hRotationSpeed = 100f;

    public float vRotationSpeed = 80f;

    // Use this for initialization

    void Start()
    {
        Cursor.visible = false;

        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);
     }

    void Update()
    {
        movement();
    }

    private void movement()
    {
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        UnityEngine.Vector3 movementDirection = hMovement * UnityEngine.Vector3.right + vMovement * UnityEngine.Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));



        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

        transform.Rotate(0f, hPlayerRotation, 0f);

        cam.transform.Rotate(-vCamRotation, 0f, 0f);
    }
}